#!/usr/bin/python

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import subprocess, time, threading


class TrayIcon(Gtk.StatusIcon):

    check_interval = 3600 # seconds

    def __init__(self):
        Gtk.StatusIcon.__init__(self)
        self.set_from_icon_name('help-about')
        self.set_has_tooltip(True)
        self.set_visible(True)
        self.connect("popup_menu", self.on_secondary_click)
        self.lastupdate = 0
        self.update_timer = threading.Timer(None, None)
        self.run_checkupdates(None)

    def on_secondary_click(self, widget, button, gtk_time):
        menu = Gtk.Menu()

        menu_item0 = Gtk.MenuItem("Last check: " + time.strftime("%a, %H:%M:%S", time.localtime(self.lastupdate)))
        menu.append(menu_item0)

        menu_item1 = Gtk.MenuItem("Run checkupdates now")
        menu.append(menu_item1)
        menu_item1.connect("activate", self.run_checkupdates, True)

        menu_item2 = Gtk.MenuItem("Quit")
        menu.append(menu_item2)
        menu_item2.connect("activate", self.quit)

        menu.show_all()
        menu.popup(None, None, None, self, 3, gtk_time)

    def restart_timer(self):
        if self.update_timer.is_alive():
            self.update_timer.cancel()

        self.update_timer = threading.Timer(self.check_interval, self.restart_timer)
        self.update_timer.start()
        self.run_checkupdates(None)

    def run_checkupdates(self, widget=None, force_update=False):
        cur_time = time.mktime(time.localtime())
        
        if cur_time > self.check_interval + self.lastupdate or force_update == True:
            self.lastupdate = cur_time
            output = subprocess.run("checkupdates", stdout=subprocess.PIPE)
            parsed = output.stdout.decode("utf-8")
            
            if parsed != "":
                self.set_from_icon_name("software-update-urgent")
                self.set_tooltip_text(parsed)
            else:
                self.set_from_icon_name('help-about')
                self.set_tooltip_text("Arch packages up to date")

            self.restart_timer()

    def quit(self, widget):
        if self.update_timer.is_alive():
            self.update_timer.cancel()

        Gtk.main_quit()


if __name__ == '__main__':
    tray = TrayIcon()
    Gtk.main()

